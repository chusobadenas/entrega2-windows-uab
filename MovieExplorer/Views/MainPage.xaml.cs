﻿using MovieExplorer.ViewModels;
using System.Windows.Input;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MovieExplorer.Views {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page {

        public MainPage() {
            this.InitializeComponent();
        }

        private void LVF_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            ((VMMain)this.DataContext).OnSelectedItem((VMItemFilm)this.LVF.SelectedItem);
        }

        private void LVS_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            ((VMMain)this.DataContext).OnSelectedItem((VMItemTvShow)this.LVS.SelectedItem);
        }

        private void fillAppBarButton(AppBarButton button, ICommand command, string label) {
            button.Label = label;
            button.Command = command;
            button.CommandParameter = label;
        }

        private void Pivot_PivotItemLoaded(Pivot sender, PivotItemEventArgs args) {
            string pivotName = args.Item.Name;
            VMMain vmMain = ((VMMain)this.DataContext);

            // Change bottom app bar actions
            if (pivotName.Equals("Movies")) {
                this.clearAppBarButton.Command = vmMain.ClearMoviesCommand;
                fillAppBarButton(this.firstAppBarButton, vmMain.AddMovieCommand, "La vita è bella");
                fillAppBarButton(this.secondAppBarButton, vmMain.AddMovieCommand, "The Truman Show");
                fillAppBarButton(this.thirdAppBarButton, vmMain.AddMovieCommand, "Inception");
                fillAppBarButton(this.fourAppBarButton, vmMain.AddMovieCommand, "Coherence");
                fillAppBarButton(this.fiveAppBarButton, vmMain.AddMovieCommand, "Django");
            }
            else if(pivotName.Equals("TVShows")) {
                this.clearAppBarButton.Command = vmMain.ClearTvShowsCommand;
                fillAppBarButton(this.firstAppBarButton, vmMain.AddTvShowCommand, "Lost");
                fillAppBarButton(this.secondAppBarButton, vmMain.AddTvShowCommand, "Prison Break");
                fillAppBarButton(this.thirdAppBarButton, vmMain.AddTvShowCommand, "Dexter");
                fillAppBarButton(this.fourAppBarButton, vmMain.AddTvShowCommand, "Breaking Bad");
                fillAppBarButton(this.fiveAppBarButton, vmMain.AddTvShowCommand, "The Big Bang Theory");
            }
        }
    }
}
