﻿
namespace MovieExplorer.ViewModels {

    public class VMItemTvShow : VMAbstractItem {

        private double rate;

        public double Rate {
            get {
                return rate;
            }
            set {
                if (value != rate) {
                    rate = value;
                    RaisePropertyChanged("Rate");
                }
            }
        }
    }
}
