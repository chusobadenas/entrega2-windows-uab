﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MovieExplorer.Views {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FilmInfoPage : Page {

        public FilmInfoPage() {
            this.InitializeComponent();
            var size = Window.Current.Bounds;

            if (size.Width <= 600) {
                Description.Width = size.Width - 20;
                Cast.Width = size.Width - 20;
                Crew.Width = size.Width - 20;
            }
            else {
                Description.Width = 600 - 20;
                Cast.Width = 600 - 20;
                Crew.Width = 600 - 20;
            }
        }
    }
}
