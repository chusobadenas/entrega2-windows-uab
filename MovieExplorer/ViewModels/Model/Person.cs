﻿using MovieExplorer.ViewModels.Base;

namespace MovieExplorer.ViewModels.Model {

    public class Person : VMBase {

        private string name;
        private string picture;

        public string Name {
            get {
                return name;
            }
            set {
                if (value != name) {
                    name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        public string Picture {
            get {
                return picture;
            }
            set {
                if (value != picture) {
                    picture = value;
                    RaisePropertyChanged("Picture");
                }
            }
        }
    }
}
