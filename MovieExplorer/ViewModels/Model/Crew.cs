﻿
namespace MovieExplorer.ViewModels.Model {

    public class Crew : Person {

        private string department;
        private string job;

        public string Department {
            get {
                return department;
            }
            set {
                if (value != department) {
                    department = value;
                    RaisePropertyChanged("Department");
                }
            }
        }

        public string Job {
            get {
                return job;
            }
            set {
                if (value != job) {
                    job = value;
                    RaisePropertyChanged("Job");
                }
            }
        }
    }
}
