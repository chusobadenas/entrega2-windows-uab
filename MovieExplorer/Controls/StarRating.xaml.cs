﻿using System;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace MovieExplorer.Controls {

    public sealed partial class StarRating : UserControl {

        public static readonly DependencyProperty RateProperty = DependencyProperty.Register("Rate", typeof(double),
            typeof(StarRating), new PropertyMetadata(0.0, new PropertyChangedCallback(OnRateChanged)));

        public double Rate {
            get { return (double)GetValue(RateProperty); }
            set { SetValue(RateProperty, value); }
        }

        private static void OnRateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e) {
            var uc = obj as StarRating;

            if (uc != null && e.NewValue != e.OldValue) {
                double value = Convert.ToDouble(e.NewValue);

                uc.star_1.Fill = new SolidColorBrush(Colors.Gray);
                uc.star_2.Fill = new SolidColorBrush(Colors.Gray);
                uc.star_3.Fill = new SolidColorBrush(Colors.Gray);
                uc.star_4.Fill = new SolidColorBrush(Colors.Gray);
                uc.star_5.Fill = new SolidColorBrush(Colors.Gray);

                if (value == 0) {
                    return;
                }

                double floorValue = Math.Floor(value);
                double realPart = value - floorValue;

                LinearGradientBrush gradient = new LinearGradientBrush();
                gradient.StartPoint = new Point(0.5, 0);
                gradient.EndPoint = new Point(1, 0);

                GradientStop first = new GradientStop();
                first.Color = Colors.Yellow;
                first.Offset = realPart;

                GradientStop second = new GradientStop();
                second.Color = Colors.Gray;
                second.Offset = realPart;

                gradient.GradientStops.Add(first);
                gradient.GradientStops.Add(second);

                if (value > 0 && value <= 2) {
                    uc.star_1.Fill = gradient;
                }
                else if (value > 2 && value <= 4) {
                    uc.star_1.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_2.Fill = gradient;
                }
                else if (value > 4 && value <= 6) {
                    uc.star_1.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_2.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_3.Fill = gradient;
                }
                else if (value > 6 && value <= 8) {
                    uc.star_1.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_2.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_3.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_4.Fill = gradient;
                }
                else if (value > 8 && value < 10) {
                    uc.star_1.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_2.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_3.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_4.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_5.Fill = gradient;
                }
                else if(value >= 10) {
                    uc.star_1.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_2.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_3.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_4.Fill = new SolidColorBrush(Colors.Yellow);
                    uc.star_5.Fill = new SolidColorBrush(Colors.Yellow);
                }
            }
        }

        public StarRating() {
            this.InitializeComponent();
        }
    }
}
