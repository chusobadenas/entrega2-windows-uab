﻿using MovieExplorer.ViewModels.Model;
using System.Collections.ObjectModel;

namespace MovieExplorer.ViewModels {

    public class VMItemFilmInfo : VMItemFilm {

        private string director;
        private string background;
        private string summary;
        private double rate;

        public ObservableCollection<Cast> CastCollection { get; set; }
        public ObservableCollection<Crew> CrewCollection { get; set; }

        public string Director {
            get {
                return director;
            }
            set {
                if (value != director) {
                    director = value;
                    RaisePropertyChanged("Director");
                }
            }
        }

        public string Background {
            get {
                return background;
            }
            set {
                if (value != background) {
                    background = value;
                    RaisePropertyChanged("Background");
                }
            }
        }

        public string Summary {
            get {
                return summary;
            }
            set {
                if (value != summary) {
                    summary = value;
                    RaisePropertyChanged("Summary");
                }
            }
        }

        public double Rate {
            get {
                return rate;
            }
            set {
                if (value != rate) {
                    rate = value;
                    RaisePropertyChanged("Rate");
                }
            }
        }

        public VMItemFilmInfo() {
            CastCollection = new ObservableCollection<Cast>();
            CrewCollection = new ObservableCollection<Crew>();
        }

        public VMItemFilmInfo(VMItemFilm film) {
            CastCollection = new ObservableCollection<Cast>();
            CrewCollection = new ObservableCollection<Crew>();

            if (film != null) {
                this.Id = film.Id;
                this.Description = film.Description;
                this.Poster = film.Poster;
                this.Title = film.Title;
                this.Year = film.Year;
            }
        }
    }
}
