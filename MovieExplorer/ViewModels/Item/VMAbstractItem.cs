﻿using MovieExplorer.ViewModels.Base;

namespace MovieExplorer.ViewModels {

    public abstract class VMAbstractItem : VMBase {

        private int id;
        private string title;
        private int year;
        private string poster;

        public int Id {
            get {
                return id;
            }
            set {
                if (value != id) {
                    id = value;
                    RaisePropertyChanged("Id");
                }
            }
        }

        public string Title {
            get {
                return title;
            }
            set {
                if (value != title) {
                    title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }

        public int Year {
            get {
                return year;
            }
            set {
                if (value != year) {
                    year = value;
                    RaisePropertyChanged("Year");
                }
            }
        }

        public string Poster {
            get {
                return poster;
            }
            set {
                if (value != poster) {
                    poster = value;
                    RaisePropertyChanged("Poster");
                }
            }
        }

        public string TitleAndDate {
            get {
                return string.Format("{0} ({1})", Title, Year.ToString());
            }
        }
    }
}
