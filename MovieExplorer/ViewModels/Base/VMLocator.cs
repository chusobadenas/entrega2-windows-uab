﻿using System;
using MovieExplorer.Services;

namespace MovieExplorer.ViewModels.Base {

    public class VMLocator {

        private INavigationService navigationService;

        private Lazy<VMMain> mainViewModel;
        private Lazy<VMFilmInfo> filmInfoViewModel;
        private Lazy<VMTvShowInfo> tvShowInfoViewModel;

        public VMLocator() {
            navigationService = new NavigationService();

            mainViewModel = new Lazy<VMMain>(() => new VMMain(navigationService));
            filmInfoViewModel = new Lazy<VMFilmInfo>(() => new VMFilmInfo(navigationService));
            tvShowInfoViewModel = new Lazy<VMTvShowInfo>(() => new VMTvShowInfo(navigationService));
        }

        public VMMain MainViewModel {
            get {
                return mainViewModel.Value;
            }
        }

        public VMFilmInfo FilmInfoViewModel {
            get {
                return filmInfoViewModel.Value;
            }
        }

        public VMTvShowInfo TvShowInfoViewModel {
            get {
                return tvShowInfoViewModel.Value;
            }
        }
    }
}
