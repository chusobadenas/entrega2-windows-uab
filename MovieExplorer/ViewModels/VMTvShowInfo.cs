﻿using MovieExplorer.ServiceReferenceTvShowInfoSearch;
using MovieExplorer.Services;
using MovieExplorer.ViewModels.Base;
using MovieExplorer.ViewModels.Model;
using System;
using System.Windows.Input;

namespace MovieExplorer.ViewModels {

    public class VMTvShowInfo : VMAbstractBase {

        #region Properties

        private VMItemTvShowInfo tvShowInfo;

        public VMItemTvShowInfo TvShowInfo {
            get {
                return tvShowInfo;
            }
            set {
                tvShowInfo = value;
                RaisePropertyChanged("TvShowInfo");
            }
        }

        #endregion

        public VMTvShowInfo(INavigationService navigationService) : base(navigationService) {}

        #region WebServices

        public async void LoadTvShowInfo(int tvShowId) {
            // Start loading
            this.ProgressBarEnabled = true;

            var client = new WSTvInfoPortTypeClient();
            GetTvShowDataByIdResponse result = await client.GetTvShowDataByIdAsync(tvShowId);

            TvShowInfo tvShowInfoResponse = result.@return;
            this.tvShowInfo = new VMItemTvShowInfo() {
                Id = tvShowInfoResponse.Id,
                Title = tvShowInfoResponse.Title,
                Status = tvShowInfoResponse.Status,
                Background = tvShowInfoResponse.Background,
                Year = tvShowInfoResponse.Year,
                Summary = tvShowInfoResponse.Summary,
                Poster = tvShowInfoResponse.Poster,
                Rate = (double)tvShowInfoResponse.Rate,
                Seasons = tvShowInfoResponse.NumSeasons,
                Episodes = tvShowInfoResponse.NumEpisodes
            };

            // Add CAST
            ActorInfo[] castArray = tvShowInfoResponse.Cast;
            if (castArray != null) {
                foreach (ActorInfo actorInfo in castArray) {
                    this.tvShowInfo.CastCollection.Add(new Cast() {
                        Name = actorInfo.Name,
                        Character = actorInfo.Character,
                        Picture = actorInfo.Picture
                    });
                }
            }

            // Add CREW
            CrewInfo[] crewArray = tvShowInfoResponse.Crew;
            if (crewArray != null) {
                foreach (CrewInfo crewInfo in crewArray) {
                    this.tvShowInfo.CrewCollection.Add(new Crew() {
                        Name = crewInfo.Name,
                        Department = crewInfo.Department,
                        Job = crewInfo.Job,
                        Picture = crewInfo.Picture
                    });
                }
            }

            // Stop loading
            this.ProgressBarEnabled = false;

            // Update changes
            UpdateAll();
        }

        #endregion

        #region Commands

        private Lazy<DelegateCommand<string>> goBack;

        public ICommand GoBackCommand {
            get {
                return goBack.Value;
            }
        }

        protected override void InitializeCommands() {
            goBack = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(GoBackCommandExecute,
                                                  GoBackCommandCanExecute));
        }

        public void GoBackCommandExecute(string param) {
            navigationService.GoBack();
        }

        public bool GoBackCommandCanExecute(string param) {
            return true;
        }

        #endregion
    }
}
