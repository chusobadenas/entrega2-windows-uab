﻿
namespace MovieExplorer.ViewModels {

    public class VMItemFilm : VMAbstractItem {

        private string description;

        public string Description {
            get {
                return description;
            }
            set {
                if (value != description) {
                    description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }
    }
}
