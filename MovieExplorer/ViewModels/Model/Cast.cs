﻿
namespace MovieExplorer.ViewModels.Model {

    public class Cast : Person {

        private string character;

        public string Character {
            get {
                return character;
            }
            set {
                if (value != character) {
                    character = value;
                    RaisePropertyChanged("Character");
                }
            }
        }
    }
}
