﻿using MovieExplorer.ViewModels;

namespace MovieExplorer.Services {

    public interface INavigationService {
        void ShowFilmInfo(VMItemFilm film);
        void ShowTvShowInfo(VMItemTvShow tvShow);
        void GoBack();
    }
}
