﻿using MovieExplorer.Services;

namespace MovieExplorer.ViewModels.Base {

    public abstract class VMAbstractBase : VMBase {

        protected INavigationService navigationService;
        protected bool progressBarEnabled;

        public bool ProgressBarEnabled {
            get {
                return progressBarEnabled;
            }
            set {
                progressBarEnabled = value;
                RaisePropertyChanged("ProgressBarEnabled");
            }
        }

        public VMAbstractBase(INavigationService navigationService) {
            this.navigationService = navigationService;
            this.ProgressBarEnabled = false;
            InitializeCommands();
        }

        protected abstract void InitializeCommands();
    }
}
