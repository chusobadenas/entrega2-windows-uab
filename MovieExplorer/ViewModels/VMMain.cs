﻿using System;
using System.Windows.Input;
using MovieExplorer.Services;
using MovieExplorer.ViewModels.Base;
using System.Collections.ObjectModel;
using MovieExplorer.ServiceReferenceFilmSearch;
using MovieExplorer.ServiceReferenceTvShowSearch;

namespace MovieExplorer.ViewModels {

    public class VMMain : VMAbstractBase {
        
        #region Properties

        private string movie;
        private string tvShow;

        public ObservableCollection<VMItemFilm> MovieCollection { get; set; }
        public ObservableCollection<VMItemTvShow> TvShowCollection { get; set; }

        public string Movie {
            get {
                return movie;
            }

            set {
                movie = value;
                RaisePropertyChanged("Movie");
                setMovieCommand.Value.RaiseCanExecuteChanged(this);
            }
        }

        public string TvShow {
            get {
                return tvShow;
            }

            set {
                tvShow = value;
                RaisePropertyChanged("TvShow");
                setTvShowCommand.Value.RaiseCanExecuteChanged(this);
            }
        }

        public string NumResultsMovies {
            get {
                if (!string.IsNullOrEmpty(movie)) {
                    return string.Format("{1} Movies that mach with '{0}'", movie, MovieCollection.Count);
                }

                return "No results.";
            }
        }

        public string NumResultsTvShows {
            get {
                if (!string.IsNullOrEmpty(tvShow)) {
                    return string.Format("{1} TV Shows that mach with '{0}'", tvShow, TvShowCollection.Count);
                }

                return "No results.";
            }
        }

        #endregion

        public VMMain(INavigationService navigationService) : base(navigationService) {
            this.MovieCollection = new ObservableCollection<VMItemFilm>();
            this.TvShowCollection = new ObservableCollection<VMItemTvShow>();
        }

        private void updateMovieProperties() {
            RaisePropertyChanged("MovieCollection");
            RaisePropertyChanged("NumResultsMovies");
            clearMoviesCommand.Value.RaiseCanExecuteChanged(this);
        }

        private void updateTvShowProperties() {
            RaisePropertyChanged("TvShowCollection");
            RaisePropertyChanged("NumResultsTvShows");
            clearTvShowsCommand.Value.RaiseCanExecuteChanged(this);
        }

        #region WebServices

        async void LoadMovies(string movieTitle) {
            // Start loading
            this.ProgressBarEnabled = true;
            MovieCollection.Clear();
            RaisePropertyChanged("NumResultsMovies");

            var client = new WSFilmSearchPortTypeClient();
            SearchMoviesByNameResponse result = await client.SearchMoviesByNameAsync(movieTitle);

            foreach (Film film in result.@return) {
                this.MovieCollection.Add(new VMItemFilm() {
                    Id = film.Id,
                    Title = film.Title,
                    Description = film.Description,
                    Year = film.Year,
                    Poster = film.Poster
                });
            }

            // Stop loading
            this.ProgressBarEnabled = false;

            // Update changes
            updateMovieProperties();
        }

        async void LoadTvShows(string tvShowTitle) {
            // Start loading
            this.ProgressBarEnabled = true;
            TvShowCollection.Clear();
            RaisePropertyChanged("NumResultsTvShows");

            var client = new WSTvSearchPortTypeClient();
            SearchTvByNameResponse result = await client.SearchTvByNameAsync(tvShowTitle);

            foreach (Tv tv in result.@return) {
                this.TvShowCollection.Add(new VMItemTvShow() {
                    Id = tv.Id,
                    Title = tv.Title,
                    Rate = (double)tv.Rate,
                    Year = tv.Year,
                    Poster = tv.Poster
                });
            }

            // Stop loading
            this.ProgressBarEnabled = false;

            // Update changes
            updateTvShowProperties();
        }

        #endregion

        #region Commands

        private Lazy<DelegateCommand<string>> setMovieCommand;
        private Lazy<DelegateCommand<string>> addMovieCommand;
        private Lazy<DelegateCommand<string>> clearMoviesCommand;

        private Lazy<DelegateCommand<string>> setTvShowCommand;
        private Lazy<DelegateCommand<string>> addTvShowCommand;
        private Lazy<DelegateCommand<string>> clearTvShowsCommand;

        public ICommand SetMovieCommand {
            get {
                return setMovieCommand.Value;
            }
        }

        public ICommand AddMovieCommand {
            get {
                return addMovieCommand.Value;
            }
        }

        public ICommand ClearMoviesCommand {
            get {
                return clearMoviesCommand.Value;
            }
        }

        public ICommand SetTvShowCommand {
            get {
                return setTvShowCommand.Value;
            }
        }

        public ICommand AddTvShowCommand {
            get {
                return addTvShowCommand.Value;
            }
        }

        public ICommand ClearTvShowsCommand {
            get {
                return clearTvShowsCommand.Value;
            }
        }

        protected override void InitializeCommands() {
            setMovieCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(SetMovieCommandExecute,
                                                  SetMovieCommandCanExecute));

            addMovieCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(AddMovieCommandExecute,
                                                  AddItemCommandCanExecute));

            clearMoviesCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(ClearMoviesCommandExecute,
                                                  ClearMoviesCommandCanExecute));

            setTvShowCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(SetTvShowCommandExecute,
                                                  SetTvShowCommandCanExecute));

            addTvShowCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(AddTvShowCommandExecute,
                                                  AddItemCommandCanExecute));

            clearTvShowsCommand = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(ClearTvShowsCommandExecute,
                                                  ClearTvShowsCommandCanExecute));
        }

        public void SetMovieCommandExecute(string param) {
            LoadMovies(param);
        }

        public bool SetMovieCommandCanExecute(object param) {
            return !string.IsNullOrEmpty(movie);
        }

        public void AddMovieCommandExecute(string param) {
            // Update title movie
            Movie = param;
            RaisePropertyChanged("Movie");

            // Update movie list
            SetMovieCommandExecute(param);
        }

        public bool AddItemCommandCanExecute(object param) {
            return !string.IsNullOrEmpty(param.ToString());
        }

        public void ClearMoviesCommandExecute(string param) {
            // Clear movie
            Movie = "";
            RaisePropertyChanged("Movie");

            // Clear the list and update
            MovieCollection.Clear();
            updateMovieProperties();
        }

        public bool ClearMoviesCommandCanExecute(object param) {
            // Only when list has movies
            return MovieCollection.Count > 0;
        }

        public void SetTvShowCommandExecute(string param) {
            LoadTvShows(param);
        }

        public bool SetTvShowCommandCanExecute(object param) {
            return !string.IsNullOrEmpty(tvShow);
        }

        public void AddTvShowCommandExecute(string param) {
            // Update title tv show
            TvShow = param;
            RaisePropertyChanged("TvShow");

            // Update tv shows list
            SetTvShowCommandExecute(param);
        }

        public void ClearTvShowsCommandExecute(string param) {
            // Clear tv show
            TvShow = "";
            RaisePropertyChanged("TvShow");

            // Clear the list and update
            TvShowCollection.Clear();
            updateTvShowProperties();
        }

        public bool ClearTvShowsCommandCanExecute(object param) {
            // Only when list has tv shows
            return TvShowCollection.Count > 0;
        }

        #endregion

        #region Callbacks

        internal void OnSelectedItem(VMItemFilm selectedItem) {
            // Navigate to film info page
            if(selectedItem != null) { 
                navigationService.ShowFilmInfo(selectedItem);
            }
        }

        internal void OnSelectedItem(VMItemTvShow selectedItem) {
            // Navigate to tv show info page
            if (selectedItem != null) {
                navigationService.ShowTvShowInfo(selectedItem);
            }
        }

        #endregion
    }
}
