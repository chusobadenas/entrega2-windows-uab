﻿using System;
using MovieExplorer.ViewModels;
using MovieExplorer.Views;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace MovieExplorer.Services {
    public class NavigationService : INavigationService {

        private Frame Frame {
            get {
                return (Frame)Window.Current.Content;
            }
        }

        private Action<NavigationEventArgs> onNavigated;

        public NavigationService() { }

        public void ShowFilmInfo(VMItemFilm film) {
            if (!this.Frame.CanGoBack) {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
                SystemNavigationManager.GetForCurrentView().BackRequested += GoBack_event;
            }

            this.onNavigated = (e) => {
                // Create film info page
                var vm = (VMFilmInfo)(e.Content as FilmInfoPage).DataContext;
                vm.FilmInfo = new VMItemFilmInfo(film);

                // Load film info
                vm.showProgressBar();
                vm.LoadMovieInfo(film.Id);
            };

            this.Frame.Navigated += RootFrame_Navigated;
            this.Frame.Navigate(typeof(FilmInfoPage));
        }

        public void ShowTvShowInfo(VMItemTvShow tvShow) {
            if (!this.Frame.CanGoBack) {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
                SystemNavigationManager.GetForCurrentView().BackRequested += GoBack_event;
            }

            this.onNavigated = (e) => {
                // Create tv show info page
                var vm = (VMTvShowInfo)(e.Content as TvShowInfoPage).DataContext;
                vm.TvShowInfo = new VMItemTvShowInfo(tvShow);

                // Load tv show info
                vm.LoadTvShowInfo(tvShow.Id);
            };

            this.Frame.Navigated += RootFrame_Navigated;
            this.Frame.Navigate(typeof(TvShowInfoPage));
        }

        void RootFrame_Navigated(Object sender, NavigationEventArgs e) {
            this.Frame.Navigated -= RootFrame_Navigated;

            if (this.onNavigated != null) {
                this.onNavigated(e);
            }

            this.onNavigated = null;
        }

        private void GoBack_event(object sender, BackRequestedEventArgs e) {
            GoBack();
            e.Handled = true;
        }

        public void GoBack() {
            if (this.Frame != null && this.Frame.CanGoBack) {
                this.Frame.GoBack();

                if (!this.Frame.CanGoBack) {
                    SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
                    SystemNavigationManager.GetForCurrentView().BackRequested -= GoBack_event;
                }
            }
        }
    }
}
