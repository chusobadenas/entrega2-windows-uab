﻿using MovieExplorer.ViewModels.Model;
using System.Collections.ObjectModel;

namespace MovieExplorer.ViewModels {

    public class VMItemTvShowInfo : VMItemTvShow {

        private string status;
        private string background;
        private string summary;
        private int seasons;
        private int episodes;

        public ObservableCollection<Cast> CastCollection { get; set; }
        public ObservableCollection<Crew> CrewCollection { get; set; }

        public string Status {
            get {
                return status;
            }
            set {
                if (value != status) {
                    status = value;
                    RaisePropertyChanged("Status");
                }
            }
        }

        public string Background {
            get {
                return background;
            }
            set {
                if (value != background) {
                    background = value;
                    RaisePropertyChanged("Background");
                }
            }
        }

        public string Summary {
            get {
                return summary;
            }
            set {
                if (value != summary) {
                    summary = value;
                    RaisePropertyChanged("Summary");
                }
            }
        }

        public int Seasons {
            get {
                return seasons;
            }
            set {
                if (value != seasons) {
                    seasons = value;
                    RaisePropertyChanged("Seasons");
                }
            }
        }

        public int Episodes {
            get {
                return episodes;
            }
            set {
                if (value != episodes) {
                    episodes = value;
                    RaisePropertyChanged("Episodes");
                }
            }
        }

        public string ShowEpisodesAndSeasons {
            get {
                return string.Format("{0} seasons\r{1} episodes", Seasons, Episodes);
            }
        }

        public VMItemTvShowInfo() {
            CastCollection = new ObservableCollection<Cast>();
            CrewCollection = new ObservableCollection<Crew>();
        }

        public VMItemTvShowInfo(VMItemTvShow tvShow) {
            CastCollection = new ObservableCollection<Cast>();
            CrewCollection = new ObservableCollection<Crew>();

            if (tvShow != null) {
                this.Id = tvShow.Id;
                this.Rate = tvShow.Rate;
                this.Poster = tvShow.Poster;
                this.Title = tvShow.Title;
                this.Year = tvShow.Year;
            }
        }
    }
}
