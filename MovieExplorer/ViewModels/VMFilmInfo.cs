﻿using MovieExplorer.ServiceReferenceFilmInfoSearch;
using MovieExplorer.Services;
using MovieExplorer.ViewModels.Base;
using MovieExplorer.ViewModels.Model;
using System;
using System.Windows.Input;
using Windows.System.Profile;
using Windows.UI;
using Windows.UI.ViewManagement;

namespace MovieExplorer.ViewModels {

    public class VMFilmInfo : VMAbstractBase {

        #region Properties

        private VMItemFilmInfo filmInfo;

        public VMItemFilmInfo FilmInfo {
            get {
                return filmInfo;
            }
            set {
                filmInfo = value;
                RaisePropertyChanged("FilmInfo");
            }
        }

        #endregion

        public VMFilmInfo(INavigationService navigationService) : base(navigationService) {}

        #region WebServices

        public async void LoadMovieInfo(int movieId) {
            // Start loading
            this.ProgressBarEnabled = true;

            var client = new WSFilmInfoPortTypeClient();
            GetMovieDataByIdResponse result = await client.GetMovieDataByIdAsync(movieId);

            FilmInfo filmInfoResponse = result.@return;
            this.filmInfo = new VMItemFilmInfo() {
                Id = filmInfoResponse.Id,
                Title = filmInfoResponse.Title,
                Director = filmInfoResponse.Director,
                Background = filmInfoResponse.Background,
                Year = filmInfoResponse.Year,
                Description = filmInfoResponse.Description,
                Summary = filmInfoResponse.Summary,
                Poster = filmInfoResponse.Poster,
                Rate = (double)filmInfoResponse.Rate
            };

            // Add CAST
            ActorInfo[] castArray = filmInfoResponse.Cast;
            if(castArray != null) {
                foreach (ActorInfo actorInfo in castArray) {
                    this.filmInfo.CastCollection.Add(new Cast() {
                        Name = actorInfo.Name,
                        Character = actorInfo.Character,
                        Picture = actorInfo.Picture
                    });
                }
            }

            // Add CREW
            CrewInfo[] crewArray = filmInfoResponse.Crew;
            if (crewArray != null) {
                foreach (CrewInfo crewInfo in crewArray) {
                    this.filmInfo.CrewCollection.Add(new Crew() {
                        Name = crewInfo.Name,
                        Department = crewInfo.Department,
                        Job = crewInfo.Job,
                        Picture = crewInfo.Picture
                    });
                }
            }

            // Stop loading
            this.ProgressBarEnabled = false;

            // Update changes
            UpdateAll();
        }

        public async void showProgressBar() {
            AnalyticsVersionInfo ai = AnalyticsInfo.VersionInfo;
            string systemFamily = ai.DeviceFamily;

            if (systemFamily == "Windows.Mobile") {
                StatusBar statusBar = StatusBar.GetForCurrentView();
                statusBar.BackgroundColor = Colors.Black;
                statusBar.ForegroundColor = Colors.Blue;
                statusBar.BackgroundOpacity = 100;
                await statusBar.ProgressIndicator.ShowAsync();
            }
        }

        #endregion

        #region Commands

        private Lazy<DelegateCommand<string>> goBack;

        public ICommand GoBackCommand {
            get {
                return goBack.Value;
            }
        }

        protected override void InitializeCommands() {
            goBack = new Lazy<DelegateCommand<string>>(
                () => new DelegateCommand<string>(GoBackCommandExecute,
                                                  GoBackCommandCanExecute));
        }

        public void GoBackCommandExecute(string param) {
            navigationService.GoBack();
        }

        public bool GoBackCommandCanExecute(string param) {
            return true;
        }

        #endregion
    }
}
